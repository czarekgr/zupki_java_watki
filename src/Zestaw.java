import java.util.*;

public class Zestaw {

	final int SUMA_MAX = 10000;
	int[] liczby;
	int n, k;
	int[] licznik;
	
	public Zestaw() {
		
	};

	public Zestaw(int[] dane) {
		this.liczby = dane;
	}

	public int wczytaj_dane() {
		liczby = new int[100];
		int ilosc = 0;
		int tmp = 0;
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {

			tmp = in.nextInt();
			if ((tmp > 0) && (tmp <= SUMA_MAX)) {
				ilosc++;
				liczby[ilosc] = tmp;
			}
		}
		System.out.println(" -------ilosc = " + ilosc);
		Arrays.sort(liczby, 1, ilosc + 1);
		for (int i = 1; i <= ilosc; i++)
			System.out.print(liczby[i] + " ");
		System.out.println();

		return ilosc;
	}

	int wywal_zaduze(int n, int k) {
		int suma, j;

		suma = 0;
		for (int i = 1; i < k; i++)
			// sumowanie k-1 najmniejszych liczb
			suma = suma + liczby[i];
		j = n;
		while (suma + liczby[j] > SUMA_MAX)
			j--;
		if (j < k)
			j = k;
		return (j);
	};

	void zeruj_licznik(int k) {
		licznik = new int[k];
		for (int i = 0; i < k; i++) {
			licznik[i] = i + 1;
		}

	}

	boolean next_licznik(int n, int k) {
		if (k > n)
			return false;

		// szukaj elementu ktory da sie powiekszyc
		// zaczynajac od ostatniego
		int i = k - 1;
		while (i >= 0) {
			// sprawdz czy element i-ty da sie powiekszyc
			if (licznik[i] < (n - k + i + 1)) { // n - (k - i - 1) - usunięty
				// nawias
				licznik[i]++;

				// ustaw kolejne elementy o 1 wieksze od poprzednich
				while ((i + 1) < k) {
					licznik[i + 1] = licznik[i] + 1;
					i++;
				}
				return true;
			}

			// przejdz do poprzedniego elementu
			i--;
		}
		return false;
	};

	int max_najmlodszy(int n, int k) {

		int suma, w = 0;

		for (int i = 1; i <= n - k + 2; i++) {

			suma = 0;
			w = i;
			for (int j = 0; j <= k - 1; j++)
				suma = suma + liczby[i + j];
			if (suma > SUMA_MAX)
				break;
		}
		w--;
		return (w);
	};

	void kombinacje(int n, int k) {
		int suma = 0;
		int mn;
		zeruj_licznik(k);
		mn = max_najmlodszy(n, k);
		System.out.println("k=" + k + "   n=" + n + "  mn=" + mn);
		mn++;
		do {
			suma = 0;
			for (int a = 0; a < k; a++) {

				suma = suma + liczby[licznik[a]];
				if (suma > SUMA_MAX)
					break; // przyspiesza ok 2 razy dla 35 liczb
			}
			if ((suma <= SUMA_MAX) && (suma % 1000) == 0) {

				for (int a = 0; a < k; a++)
					System.out.print("+" + liczby[licznik[a]]);
				System.out.println("=" + suma);

			}
		} while (next_licznik(n, k) && licznik[0] < mn);
	};

	int max_k(int n) {

		int k, suma;

		k = 0;
		suma = 0;
		do {
			k++;
			suma = suma + liczby[k];
		} while ((suma < SUMA_MAX) && (k < n));
		return (k - 1);
	};

}
