public class zupki {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */

	public static void main(String[] args) throws InterruptedException {
		int n, n_max, max_k;
		Thread[] t =new Thread[100];
		
		Zestaw liczby = new Zestaw();
		n = liczby.wczytaj_dane();
		max_k = liczby.max_k(n);
		System.out.println(" ilość wczytanych " + n);
		System.out.println(" max_k = " + max_k);

		for (int k = 1; k <= max_k; k++) {
			// System.out.println("Badanie zestawów " + k + " elementowych");
			n_max = liczby.wywal_zaduze(n, k);

            tworz_watek w = new tworz_watek(liczby.liczby,n_max,k);			
			t[k] = new Thread(w);
			t[k].start();
			}
		for (int k = 1; k <= max_k; k++) {
			t[k].join();
		}
		
		System.out.println("------------ Koniec obliczeń ---------------");
	}

}
